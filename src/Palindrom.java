import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.Stack;

public class Palindrom {
	 // this is the main methods
	 public static void main(String[ ] args)
	   {
		 
		Scanner scan = new Scanner(System.in); 
		String inputString;  String rev = "";
		
	      	System.out.print("Enter the input string : ");
	        inputString = scan.next( );

		if (isPalindrome( inputString ))
		{
			System.out.println(inputString +rev+ " is a palindrome.");
		}
		else
		{
			System.out.println(inputString+ " is not a palindrome.");
	     }
		
	   }

	   public static boolean isPalindrome(String input) 
	   
	   {   
	      Queue<Character> q = new LinkedList<Character>( );
	      Stack<Character> s = new Stack<Character>( );
	      char letter;
	      int i;
	      
	      for (i = 0; i < input.length( ); i++)
	      {
		 letter = input.charAt(i);
	         q.add(letter);
	         s.push(letter);
	      }    
	      while (!q.isEmpty( ))
	      {
	         if (q.remove( ) != s.pop( ))
	           return false;
	      }
	      return true; 
	   }
	    
	
	

}
